import numpy as np
import matplotlib.pyplot as plt
import datetime as datetime
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA
import matplotlib.pyplot as plt
import ms_loader as loader
import TGAMS_toolkit as tm

def show_time_temperature(tga,ms):
	tga_temp = tga.data_dict['Temperature']
	tga_time = tga.data_dict['Time']
	ms_temp = ms.data_dict[ms.channel_dict['T']]
	ms_time = np.asarray(ms.time)/60
	fig = plt.figure(figsize=(6,4))
	ax1 = fig.add_subplot(111)
	ax2 = ax1.twiny()
	ax1.plot(tga_time,tga_temp,color='r')
	ax2.plot(ms_time,ms_temp,color='b')
	plt.show()

def format_ms_ax(ax,axdata):
	pow1 = tm.get_power(axdata)
	labelstring = '^{\mathregular{'+str(pow1)+'}}'
	ax.set_ylabel(r'MS signal (10$'+labelstring+'$)')

def format_ax_color(ax1,ax2,ax3):
	ax1.axis["left"].label.set_color('blue')
	ax2.axis["right"].label.set_color('green')
	ax3.axis["right"].label.set_color('red')

def fix_mass_axis(ax3):
	offset = 60
	new_fixed_axis = ax3.get_grid_helper().new_fixed_axis
	ax3.axis["right"] = new_fixed_axis(loc="right",axes=ax3,offset=(offset, 0))
	ax3.axis["right"].toggle(all=True)

def plot(ms,tga,ms_lim,tga_lim,gas1,gas2):
	t = ms.time
	gas1_ms = ms.data_dict[ms.channel_dict[gas1]][ms_lim:]
	gas2_ms = ms.data_dict[ms.channel_dict[gas2]][ms_lim:]
	T = ms.data_dict[ms.channel_dict['T']][ms_lim:]
	fig = plt.figure(figsize=(6,4))
	ax1 = host_subplot(111, axes_class=AA.Axes)
	plt.subplots_adjust(right=0.75)
	p1, = ax1.plot(T,gas1_ms,label=label_dict[gas1])
	format_ms_ax(ax1,gas1_ms)
	ax1.set_xlabel(r'Temperature $^{\mathregular{o}}$C')
	ax2 = ax1.twinx()
	p2, = ax2.plot(T,gas2_ms,ls=':',label=label_dict[gas2],color='green')
	format_ms_ax(ax2,gas2_ms)
	ax3 = ax1.twinx()
	TGA_mass = tga.data_dict['Mass_loss'][tga_lim:]
	TGA_temperature = tga.data_dict['Temperature'][tga_lim:]
	p3, = ax3.plot(TGA_temperature,TGA_mass,color='red',label='Mass')
	fix_mass_axis(ax3)
	format_ax_color(ax1,ax2,ax3)
	ax3.set_ylabel('Mass [%]')
	ax1.legend(loc='center left',frameon=False)
	plt.draw()
	plt.tight_layout()
	plt.savefig('Figure.png',dpi=300)

label_dict = {'CO2':'CO$_{\mathregular{2}}$','H2O':'H$_{\mathregular{2}}$O','CO':'CO'}

ms_filename = 'MS1.txt'
tga_filename = 'TGA1.txt'
ms = loader.MS(ms_filename)
tga = loader.TGA(tga_filename)

ms_lim,tga_lim = tm.get_idx_time(tga,ms,t=0)#find the index of the data at time t, useful in case of a cycled program and parts before time t shall be ignored in the plot
plot(ms,tga,ms_lim,tga_lim,'CO2','H2O')
