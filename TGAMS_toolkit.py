import numpy as np
import datetime as datetime

def convert_num_to_pot(number):
	x = np.floor(np.log10(np.abs(number)))
	return(int(x))

def get_power(ms):
	'''Returns the power of the maximum value in ms as an int'''
	ms_max = np.max(ms)
	power = convert_num_to_pot(ms_max)
	return(power)

def get_idx_time(tga,ms,t=0):
	'''Return the indices of the first time point after time t in min'''
	ms_idx = np.where(np.asarray(ms.time)>t*60)[0][0]
	tga_idx = np.where(np.asarray(tga.data_dict['Time'])>t)[0][0]
	return(ms_idx,tga_idx)

def get_idx_T(T_list,T):
	'''Return the index of the first instance in T_list where T_list[i] > T'''
	idx = np.where(T_list>T)[0][0]
	return(idx)

def get_mass_at_T(mass_list,T_list,T):
	'''Return the mass at temperature T'''
	idx = get_idx_T(T_list,T)
	mass = mass_list[idx]
	return(mass)

