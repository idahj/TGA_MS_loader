This is a simple script for plotting data from the Netzsch TGA/DCS and Aerlos MS instrument.
To use the program, the datafiles must be exported as .txt files with UTF-8 encoding. 
For the MS-data, simply export as ASCII-file and save as .txt in an editor.
TGA1.txt and MS1.txt are included as demonstrations files, to plot your own data change the file names in tga_ms_plotter.py.
tga_ms_plotter.py imports TGAMS_toolkit.py and ms_loader.py.

By default, CO2, H2O and CO can be plotted.
To add other masses, use the function loader.add_gas(Mass_list,name_list), which  accepts list of strings.
Mass must be written as in the ms file.
Ex: add_gas(['44.00'],['CO2']) and update the ms and tga object by running again:
ms = loader.MS(ms_filename)
tga = loader.TGA(tga_filename)


