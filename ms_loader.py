import numpy as np
import datetime as datetime

class MS:
	def __init__(self,filename):
		self.filename = filename
		self.sample_cycles= 'default.mdc'
		self.rec_time = datetime.datetime.now()
		self.nr_cycles = 0
		self.cycle_start = 1
		self.cycle_end =	1
		self.datablocks = 3
		self.data_dict = {}
		self.data_startline = 0
		self.data_idx = {}
		self.time = []
		self.channel_dict = {}
		self.load_header()
		self.load_data()

	def set_labels(self,line):
		for idx,element in enumerate(line):
			self.data_idx[element] = idx
		print('Labels set')

	def load_header(self):
		f=open(self.filename,'r')
		lines = f.readlines()
		self.sample_cycles = lines[0].split('\t')[-1].split('\n')[0]
		date = lines[1].split('\t')[1]
		time = lines[1].split('\t')[-1].split('\n')[0]
		self.rec_time = datetime.datetime.strptime(date+'-'+time,'%d.%m.%Y-%H:%M:%S')
		self.nr_cycles = int(lines[4].split('\t')[-1].split('\n')[0])
		self.cycle_start = int(lines[5].split('\t')[-1].split('\n')[0])
		self.cycle_end = int(lines[6].split('\t')[-1].split('\n')[0])
		self.datablocks = int(lines[7].split('\t')[-1].split('\n')[0])
		idx = 9
		data_line = False
		self.data_dict = {}
		while data_line==False:
			line = lines[idx].split('\t')
			if line[0][:-2] == 'Datablock':
				data_key = line[-3]
			elif len(line[0]) > 2 and line[0][2] == '/':
				channel = line[0]
				measure = line[1]
				self.data_dict[channel] = []
				if measure == 'Temp [C]' or measure == 'Temp [°C]':
					measure = 'T'
				elif measure == '[V] TC':
					measure = 'V'
				elif measure in gas_dict.keys():
					measure = gas_dict[measure]
				self.channel_dict[measure] = channel
			elif line[0] == 'Cycle':
				self.set_labels(line)
				data_line=True
				self.data_startline = idx+1
			idx +=1
		f.close()
		return()

	def load_data(self):
		f=open(self.filename,'r')
		for line in f.readlines()[self.data_startline:-1]:
			line = line.split('\t')
			for label in  self.data_dict.keys():
				idx = int(self.data_idx[label])
				self.data_dict[label].append(float(line[idx]))
			time_idx = int(self.data_idx['RelTime[s]'])
			self.time.append(float(line[time_idx]))
		f.close()
		return()

class TGA:
	def __init__(self,filename):
		self.filename = filename
		self.data_dict = {}
		self.load_data()

	def load_data(self):
		data_array = np.loadtxt(self.filename,delimiter=';')
		col_label = ['Temperature','Time','DCS','Mass_loss','Sensitivity','segment']
		i=0
		for column in data_array.T:
		    label = col_label[i]
		    self.data_dict[label] = column;
		    i = i+1

def add_gas(Mw,name):
	'''add_gas(Mass_list,name_list) accepts list of strings. Mass must be written as in the ms file. Ex: add_gas(['44.00'],['CO2'])'''
	idx=0
	for val in Mw:
		gas_dict[val]=name[idx]
		idx+=1

gas_dict = {'44.00':'CO2','18.00':'H2O','28.00':'CO'}

